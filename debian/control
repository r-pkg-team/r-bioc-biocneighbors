Source: r-bioc-biocneighbors
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-biocneighbors
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-biocneighbors.git
Homepage: https://bioconductor.org/packages/BiocNeighbors/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp,
               r-bioc-assorthead,
               architecture-is-64-bit
Testsuite: autopkgtest-pkg-r

Package: r-bioc-biocneighbors
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Nearest Neighbor Detection for Bioconductor Packages
 Implements exact and approximate methods for nearest neighbor
 detection, in a framework that allows them to be easily switched within
 Bioconductor packages or workflows. Exact searches can be performed using
 the k-means for k-nearest neighbors algorithm or with vantage point trees.
 Approximate searches can be performed using the Annoy or HNSW libraries.
 Searching on either Euclidean or Manhattan distances is supported.
 Parallelization is achieved for all methods by using BiocParallel. Functions
 are also provided to search for all neighbors within a given distance.
